Attribute VB_Name = "RenewalCreationAssistant"
Option Explicit

Global TableName As String, ColumnName As String, Rule As String, Scorecard As String, ScorecardDB As String, ScorecardLastRun As String, BAName As String, SubjectArea As String, InitStartRow As Integer, InitEndRow As Integer
Global DPResultStatus() As String, TableNames() As String, SharePointPath As String, ConnectionString As String
Global SORName(16) As String, SORNumber(16) As String
Global Counter As Integer
Global SourceTemplate As Boolean, ModifyQuery As Boolean, Protegrity As Boolean
Global SQLConnection As ADODB.Connection
Dim ScreenShotPath As String, SourceFileName As String, DestinationFileName As String, Query As String, Schema As String, SummarySheet As String

Sub Main()

    Application.CutCopyMode = False
    Application.ScreenUpdating = False
    Application.DisplayAlerts = False
'    On Error GoTo ErrHandler
    
    SourceTemplate = True
    ModifyQuery = False
    Protegrity = False
    
'   Application.FileDialog(msoFileDialogOpen).AllowMultiSelect = False
'   ScreenShotPath = Application.FileDialog(msoFileDialogOpen).Show

    Call ReadData
    
    Schema = OpenConnection()
    
    Call CheckSourceTemplate(SubjectArea)
    
    Call AssignArrays
    
    SourceFileName = SelectSampleTemplate(SubjectArea, SourceTemplate, Rule)
    
    DestinationFileName = OpenWorkbook(SharePointPath, SourceFileName, Scorecard, ColumnName, Rule)
    
    With Application.Workbooks(DestinationFileName)
        
        Call EditTemplate(TableName, ColumnName, ScorecardDB, ScorecardLastRun, BAName)
        
        Call EditScreenshot
        
        For Counter = 0 To UBound(TableNames)
        
            Query = ReformatQuery(TableName, ColumnName)
            
            Call RunQuery(Query, SubjectArea)
            
        Next
        
    End With
    
    Call InsertRows
    
    Call GetDPResultStatus
    
    Application.Workbooks(2).Save
    Application.Workbooks(2).Close
    
    Application.ScreenUpdating = True
    
'ErrHandler:
'
'    Debug.Print Err.Number & ": " & Err.Description
'
'    Err.Clear
'
'    Resume Next

End Sub

Sub ReadData()

    Dim MetricData As String, SplitMetricData() As String
    
    With Sheets("Sheet1")
        
'       Read table name, column name, rule, scorecard data, metric data, analyst name
        BAName = .Range("F28")
        ScorecardLastRun = .Range("F20")
        Scorecard = .Range("F4")
        ScorecardDB = .Range("F24")
        TableName = Replace(Scorecard, "_NC", "")
        ColumnName = .Range("F8")
        Rule = .Range("F12")
        MetricData = .Range("F16")
        
'       Parse metric data
        SplitMetricData = Split(MetricData, Chr(10))
        ReDim DPResultStatus(0 To 1, 0 To UBound(SplitMetricData) / 12)
        
'       For Counter = 0 To UBound(DPResultStatus, 2)
'
'           DPResultStatus(0, Counter) = SplitMetricData((Counter + 1) * (12))
'           DPResultStatus(1, Counter) = Trim(SplitMetricData((Counter + 1) * (12) + 3))
'
'       Next
        
'       Determine if table is in claims or membership
        If InStr(TableName, "CLM") <> 0 _
        Or TableName = "MEDCR_PART_D" _
        Then
        
            SubjectArea = "Claims"
            
        Else
        
            SubjectArea = "Membership"
            
        End If
        
'       TableName = TextBox1.Value
'       TableName = SplitSourceFileName(2)
'       ColumnName = TextBox2.Value
'       ColumnName = SplitSourceFileName(4)
'       Rule = "UNK NA"
'       Rule = SplitSourceFileName(6)
        
    End With

End Sub

Function OpenConnection() As String

'   Establish Teradata connection
    Set SQLConnection = CreateObject("ADODB.Connection")
'    ConnectionString = "SessionMode=Teradata;Driver=Teradata;Authentication=LDAP"
    SQLConnection.Properties("Prompt") = 1
    SQLConnection.ConnectionTimeout = 60
    SQLConnection.CommandTimeout = 10
    SQLConnection.CursorLocation = adUseClient
    SQLConnection.Open "Authentication=LDAP"
    
'    SQLUserID = SQLConnection.Attributes

'   Determine correct database for running queries
    If InStr("DWTEST2_LDAP", SQLConnection) <> 0 _
    Then

        Schema = "T10_EDW_ALLPHI"

    Else

        Schema = "EDW_ALLPHI"

    End If
    
'   Return database
    OpenConnection = Schema

End Function

Sub CheckSourceTemplate(SubjectArea As String)

    On Error GoTo ErrHandler

    Dim Query As String
    Dim SQLCommand As ADODB.Command
    Dim SQLRecordset As ADODB.RecordSet

    If SubjectArea = "Claims" Then
    
'       Set folder path for sample Claims templates
'        SharePointPath = "https://share.antheminc.com/teams/EDWard-Data-Qua/Shared%20Documents/DATA%20PROFILING/Analyst%20Documentation/Samples/Claims%20Samples/Claims%20Samples%20V4/"
        SharePointPath = "C:\Users\AF19264\OneDrive - Anthem\Desktop\Data Profiling\Sample Templates\V4.6\Claims\"
        
'       Set query to check for source data and replace placeholders with target table and column names if table is in Claims
        Query = "select CLM_SOR_CD, COLUMNNAME, src_COLUMNNAME, count(distinct CLM_ADJSTMNT_KEY) as DistinctClaimCount from TABLENAME c inner join EDW_LOAD_LOG ELL on c.LOAD_LOG_KEY = ELL.LOAD_LOG_KEY where ELL.PBLSH_DTM >= ((date-90)-(date-90) mod 100) + 1 and CLM_SOR_CD in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104') group by 1, 2, 3 order by 1"
        Query = Replace(Query, "COLUMNNAME", ColumnName)
        Query = Replace(Query, "TABLENAME", TableName)
        
    ElseIf SubjectArea = "Membership" Then
    
'       Set folder path for sample Membership templates
'        SharePointPath = "https://share.antheminc.com/teams/EDWard-Data-Qua/Shared%20Documents/DATA%20PROFILING/Analyst%20Documentation/Samples/Membership%20Samples%20V4/"
        SharePointPath = "C:\Users\AF19264\OneDrive - Anthem\Desktop\Data Profiling\Sample Templates\V4.6\Membership\"
        
'       Set query to check for source data and replace placeholders with target table and column names if table is in Membership
        Query = "select MBRSHP_SOR_CD, COLUMNNAME, SRC_COLUMNNAME, count(*) from TABLENAME where MBRSHP_SOR_CD in ('808', '809', '815', '822', '823', '824', '886', '891', '892', '896', '994', '1104') and RCRD_STTS_CD in ('ACT', 'MIG') group by 1, 2, 3 order by 1"
        Query = Replace(Query, "COLUMNNAME", ColumnName)
        Query = Replace(Query, "TABLENAME", TableName)
        
    End If
    
    Set SQLRecordset = CreateObject("ADODB.RecordSet")
    If SQLRecordset.State = 1 Then SQLRecordset.Close
    SQLRecordset.CursorLocation = adUseClient
    SQLRecordset.Open Query, SQLConnection
    
'   Check for source data
'    Set SQLCommand = New ADODB.Command
'    Set SQLCommand.ActiveConnection = SQLConnection
'
'    With SQLCommand
'
'        .CommandTimeout = 2000
'        .CommandText = Query
'        .CommandType = adCmdText
'
'        Set SQLRecordSet = SQLCommand.Execute
'
'    End With
    
ErrHandler:

    Debug.Print Err.Number & ": " & Err.Description
    
    If Err.Number = 3709 Then
    
        SourceTemplate = False
        Err.Clear
        Resume Next
    
    End If
    
    If Err.Number = -2147467259 Then
    
        SourceTemplate = False
        Err.Clear
        Resume Next
    
    End If
    
    Debug.Print Query

End Sub

Sub AssignArrays()

'   Populate array with SOR names and numbers
    SORName(0) = "CAREMARK"
    SORName(1) = "GBD"
    SORName(2) = "WGS"
    SORName(3) = "CS90"
    SORName(4) = "STAR_ISG"
    SORName(5) = "ACES"
    SORName(6) = "FACETS"
    SORName(7) = "NASCO"
    SORName(8) = "WDS"
    SORName(9) = "CHIPS"
    SORName(10) = "VADW_PARPPO_MBRSHP"
    SORName(11) = "VADW_ITS_HOST"
    SORName(12) = "FACETS_MED_D"
    SORName(13) = "DECARE"
    SORName(14) = "FEP"
    SORName(15) = "ESI"
    SORName(16) = "CAREMORE"
    SORNumber(0) = "1037"
    SORNumber(1) = "1104"
    SORNumber(2) = "808"
    SORNumber(3) = "809"
    SORNumber(4) = "815"
    SORNumber(5) = "822"
    SORNumber(6) = "823"
    SORNumber(7) = "824"
    SORNumber(8) = "863"
    SORNumber(9) = "869"
    SORNumber(10) = "886"
    SORNumber(11) = "889"
    SORNumber(12) = "891"
    SORNumber(13) = "892"
    SORNumber(14) = "896"
    SORNumber(15) = "898"
    SORNumber(16) = "994"
    
'   Resize arrays for correct number of tables for Claims
    If SubjectArea = "Claims" Then
    
        ReDim TableNames(0 To 5) As String
    
'       Populate arrays with correct cell ranges for locating queries and tables for Claims
        If SourceTemplate = True _
        Then
        
            TableNames(0) = "Table1"
            TableNames(1) = "Table3"
            TableNames(2) = "Table4"
            TableNames(3) = "Table2"
            TableNames(4) = "Table5"
            TableNames(5) = "Table6"
        
        ElseIf Rule = "Zero" Then
        
            TableNames(0) = "Table1"
            TableNames(1) = "Table3"
            TableNames(2) = "Table4"
            TableNames(3) = "Table28"
            TableNames(4) = "Table59"
            TableNames(5) = "Table610"
            
        Else
    
            TableNames(0) = "Table1"
            TableNames(1) = "Table3"
            TableNames(2) = "Table4"
            TableNames(3) = "Table2"
            TableNames(4) = "Table5"
            TableNames(5) = "Table6"
        
        End If
        
'   Resize arrays for correct number of tables for Membership
    ElseIf SubjectArea = "Membership" Then
    
        ReDim TableNames(0 To 1) As String
        
'       Populate arrays with correct cell ranges for locating queries and tables for Membership
        If SourceTemplate = True _
        Then
        
            TableNames(0) = "Table1"
            TableNames(1) = "Table2"
        
        Else
    
            TableNames(0) = "Table1"
            TableNames(1) = "Table2"
        
        End If
        
    End If

End Sub

Function SelectSampleTemplate(SubjectArea As String, SourceTemplate As Boolean, Rule As String) As String

'   Choose filename for correct sample template
    Select Case SubjectArea
    
        Case Is = "Claims"
            
            Select Case SourceTemplate
            
                Case Is = True
                
                    Select Case Rule
                            
                        Case Is = "Completeness"
                        
                            SourceFileName = "SRC_COLUMNNAME - Completeness"
                            
                        Case Is = "Reference Check"
                        
                            SourceFileName = "SRC_COLUMNNAME - Reference Check"
                            
                        Case Is = "UNK NA"
                        
                            SourceFileName = "SRC_COLUMNNAME - UNK NA"
                            
                    End Select
                    
                Case Is = False
                
                    Select Case Rule
                    
                        Case Is = "ALL RESULTS - Check All Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - ALL RESULTS - Check All Invalid Char"
                            
                        Case Is = "ALL RESULTS - Check Limited Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - ALL RESULTS - Check Limited Invalid Char"
                            
                        Case Is = "Completeness"
                        
                            SourceFileName = "COLUMNNAME - Completeness"
                            
                        Case Is = "Filler"
                        
                            SourceFileName = "COLUMNNAME - Filler"
                            
                        Case Is = "LIMITED RESULTS - Check All Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - LIMITED RESULTS - Check All Invalid Char"
                            
                        Case Is = "LIMITED RESULTS - Check Limited Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - LIMITED RESULTS - Check Limited Invalid Char"
                            
                        Case Is = "Reference Check"
                        
                            SourceFileName = "COLUMNNAME - Reference Check"
                            
                        Case Is = "UNK NA"
                        
                            SourceFileName = "COLUMNNAME - UNK NA"
                            
                        Case Is = "Zero"
                        
                            SourceFileName = "COLUMNNAME - Zero"
                            
                    End Select
                    
            End Select
            
        Case Is = "Membership"
            
            Select Case SourceTemplate
            
                Case Is = True
                
                    Select Case Rule
                            
                        Case Is = "Completeness"
                        
                            SourceFileName = "SRC_COLUMNNAME - Completeness"
                            
                        Case Is = "Reference Check"
                        
                            SourceFileName = "SRC_COLUMNNAME - Reference Check"
                            
                        Case Is = "UNK NA"
                        
                            SourceFileName = "SRC_COLUMNNAME - UNK NA"
                        
                    End Select
                    
                Case Is = False
                
                    Select Case Rule
                    
                        Case Is = "Check All Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - ALL RESULTS - Check All Invalid Char"
                            
                        Case Is = "Check Limited Invalid Char"
                        
                            SourceFileName = "COLUMNNAME - ALL RESULTS - Check Limited Invalid Char"
                            
                        Case Is = "Completeness"
                        
                            SourceFileName = "COLUMNNAME - Completeness"
                            
                        Case Is = "Filler"
                        
                            SourceFileName = "COLUMNNAME - Filler"
                            
                        Case Is = "Reference Check"
                        
                            SourceFileName = "COLUMNNAME - Reference Check"
                            
                        Case Is = "UNK NA"
                        
                            SourceFileName = "COLUMNNAME - UNK NA"
                            
                        Case Is = "Zero"
                        
                            SourceFileName = "COLUMNNAME - Zero"
                            
                        Case Is = "FN_LN Equal"
                        
                            SourceFileName = "FN_LN Equal"
                        
                        Case Is = "FN_LN Length"
                        
                            SourceFileName = "FN_LN Length"
                        
                        Case Is = "FN_LN Populated"
                        
                            SourceFileName = "FN_LN Populated"
                        
                    End Select
                    
            End Select
            
    End Select
    
    SourceFileName = "Data Profiling SCORECARDNAME - " & SourceFileName & ".xlsm"
    
'   Return filename
    SelectSampleTemplate = SourceFileName
    
End Function

Function OpenWorkbook(SharePointPath, SourceFileName As String, Scorecard As String, ColumnName As String, Rule As String) As String

    Dim DestinationFilePath As String, SourceFileShortName As String, DestinationFileName As String, DestinationFileShortName As String
    
'    DoCMD.SetWarnings False
'    DoCMD.SetWarnings True
'    Application.ActiveWorkbook.Path
    
'   Set sample template filepath
    DestinationFilePath = "C:\Users\AF19264\OneDrive - Anthem\Desktop\"
    
'   Open correct sample template
    Application.Workbooks.Open SharePointPath & SourceFileName, UpdateLinks:=False
    
'   Assemble correct template filename
    DestinationFileName = "Data Profiling " & Scorecard & " - " & ColumnName & " - " & Rule & " - " & Format(Date, "mmmm yyyy") & ".xlsm"
    
'   Save copy of sample template with correct filename
    Application.Workbooks(2).SaveAs Filename:=DestinationFilePath & DestinationFileName, FileFormat:=xlOpenXMLWorkbookMacroEnabled
    
'   Close sample template
    Application.Workbooks(2).Close
    
'   Open saved template
    Application.Workbooks.Open DestinationFilePath & DestinationFileName, UpdateLinks:=False
    
'   Return saved template filename
    OpenWorkbook = DestinationFileName
    
End Function

Sub EditTemplate(TableName As String, ColumnName As String, ScorecardDB As String, ScorecardLastRun As String, BAName As String)

    Dim CellText As String, TextLine As String, CellTextSplit() As String, SplitTableName() As String, DPResultStatus() As String
    Dim Counter As Integer, SORCounter As Integer, TextPos As Integer
    
    With Sheets("Profiling Analysis Overview")
        
'       Replace placeholder in overview tab with column name while preserving cell formatting
        CellText = .Range("A11").Value
        TextPos = InStr(CellText, "COLUMNNAME")
'        .Range("A11").Characters(TextPos, 10).Insert (ColumnName)
        
'       Write today's date to overview tab
        .Range("A14").Value = "Date of Basic Queries Ran: " & CStr(Format(Date, "mm/dd/yyyy"))
        
'       Write most recent scorecard run date to overview tab
        If ScorecardDB = "" Then ScorecardDB = "PROD"
        If ScorecardLastRun = "" Then ScorecardLastRun = "00/00/2019"
        
        .Range("A15").Value = "Date of " & ScorecardDB & " Scorecard Run: " & Format(ScorecardLastRun, "mm/dd/yyyy")
        
'       Write table name to overview tab
        .Range("F15").Value = TableName
        
'       Write name to overview tab
        If BAName <> "" Then .Range("I14").Value = BAName
        
        .Range("J14").Value = Replace(.Range("J14").Value, "Currently", "Assigned")
        
'       Write data profiling results to overview tab
'       For Counter = 0 To 12
'
'           If TextLine = SORName(Counter) & "_" & SORNumber(Counter) & "_" & ColumnName & "_" & Rule Then
'
'           End If
'
'       Next

'        .Range("D13").Value = .Range("D13").Value & " " & CStr(Format(Date, "mm/dd/yy"))
        .Range("E13").Value = "Data Profiling Previous Result Status"

    End With
    
    If Rule = "UNK NA" And SourceTemplate = True Then
    
        SummarySheet = "Summary - ALL SOURCES " & Rule
    
    Else
    
        SummarySheet = "Summary - ALL SOURCES"
        
    End If
    
    With Sheets(SummarySheet)
    
        .Range("A4").Value = Replace(.Range("A4").Value, "? " & Chr(10), "?" & Chr(10))
        .Range("A4").Value = Replace(.Range("A4").Value, ". " & Chr(10), "." & Chr(10))
        
    End With
    
    For Counter = 1 To Sheets.Count
    
        If Sheets(Counter).Name = "CAREMARK_1037" Then
        
            With Sheets("CAREMARK_1037")
            
                .Select
                ActiveWindow.Zoom = 100
                .Range("A4").Value = Replace(.Range("A4").Value, "1. T", "1.  T")
                .Range("A4").Value = Replace(.Range("A4").Value, "2. N", "2.  N")
                .Range("A4").Value = Replace(.Range("A4").Value, "2.  No further profiling is required at this time.", "2.  No further profiling for this source/metric is needed for this CDE/Metric/Source.")
                .Range("A4").Value = Replace(.Range("A4").Value, ".  " & Chr(10), "." & Chr(10))
                .Range("A4").Value = Replace(.Range("A4").Value, ". " & Chr(10), "." & Chr(10))
                
            End With
            
        End If
        
    Next
    
    For Counter = 1 To Sheets.Count
    
        For SORCounter = 1 To 16
        
            If Sheets(Counter).Name = SORName(SORCounter) & "_" & SORNumber(SORCounter) Then
            
                With Sheets(SORName(SORCounter) & "_" & SORNumber(SORCounter))
                
                    .Select
                    ActiveWindow.Zoom = 100
                    .Range("A4").Value = Replace(.Range("A4").Value, "1. T", "1.  T")
                    .Range("A4").Value = Replace(.Range("A4").Value, "2. N", "2.  N")
                    .Range("A4").Value = Replace(.Range("A4").Value, "2.  No further profiling is required at this time.", "2.  No further profiling for this source/metric is needed for this CDE/Metric/Source.")
                    .Range("A4").Value = Replace(.Range("A4").Value, ".  " & Chr(10), "." & Chr(10))
                    .Range("A4").Value = Replace(.Range("A4").Value, ". " & Chr(10), "." & Chr(10))
                    
                End With
                
            End If
            
        Next
        
    Next
    
End Sub

Sub EditScreenshot()

    Dim PictureLeft As Integer, PictureTop As Integer, ExtraRow As Integer
    Dim FullFilePathScreenShot As String
    Dim Shape As Shape
    
    If SubjectArea = "Claims" Then ExtraRow = 2
    
    With Sheets("Profiling Analysis Overview")
    
        PictureLeft = .Range("B30").Left
        PictureTop = .Range("B" & (30 + ExtraRow)).Top
            
        For Each Shape In Worksheets("Profiling Analysis Overview").Shapes

            If Shape.Type = 11 Then Shape.Delete
            
        Next
        
        FullFilePathScreenShot = "C:\Users\AF19264\Pictures\" & Scorecard & " - " & ColumnName & " - " & Rule & ".png"
        
        If Dir(FullFilePathScreenShot) <> "" Then
        
            .Shapes.AddPicture _
            Filename:=FullFilePathScreenShot, _
            LinkToFile:=msoFalse, SaveWithDocument:=msoTrue, _
            Left:=PictureLeft, Top:=PictureTop, Width:=-1, Height:=-1
            
        End If
        
    End With
    
End Sub

Function ReformatQuery(TableName As String, ColumnName As String)

    Dim TableStartPos As Range, QueryCell As Range

    With Sheets(SummarySheet)
    
    Set TableStartPos = .ListObjects(TableNames(Counter)).Range.Item(1)
    Set QueryCell = .Range(TableStartPos.Offset(-1, 0).Address)
    
'       Edit query text
        QueryCell.Value = Replace(QueryCell.Value, "  ", " ")
        QueryCell.Value = Replace(QueryCell.Value, ",'", ", '")
        QueryCell.Value = Replace(QueryCell.Value, "' )", "')")
        QueryCell.Value = Replace(QueryCell.Value, ",2", ", 2")
        QueryCell.Value = Replace(QueryCell.Value, ",3", ", 3")
        QueryCell.Value = Replace(QueryCell.Value, "(( ", "((")
        QueryCell.Value = Replace(QueryCell.Value, "n  E", "n E")
        QueryCell.Value = Replace(QueryCell.Value, "=  ((", "= ((")
        QueryCell.Value = Replace(QueryCell.Value, ")>", ") > ")
        QueryCell.Value = Replace(QueryCell.Value, ")<", ") < ")
        QueryCell.Value = Replace(QueryCell.Value, "sel ", "select ")
        QueryCell.Value = Replace(QueryCell.Value, " --", "  --")
        QueryCell.Value = Replace(QueryCell.Value, "AS TOTAL_COUNT", "as TotalCount")
        QueryCell.Value = Replace(QueryCell.Value, "--inner join EDW_LOAD_LOG ELL ", "--inner join EDW_LOAD_LOG ELL")
        
'
        If SubjectArea = "Claims" Then
        
            QueryCell.Value = Replace(QueryCell.Value, "(*)", "(distinct CLM_ADJSTMNT_KEY)")
            QueryCell.Value = Replace(QueryCell.Value, "COUNT(DISTINCT", "count(distinct")
            QueryCell.Value = Replace(QueryCell.Value, "AS DISTINCT_CLAIM_COUNT", "as DistinctClaimCount")
            QueryCell.Value = Replace(QueryCell.Value, "clm_sor_cd", "CLM_SOR_CD")
            QueryCell.Value = Replace(QueryCell.Value, "-- CLM table only", "--CLM table only")
            
            If TableName = "CLM_PAID" And Counter < 3 Then
            
'                QueryCell.Value = Replace(QueryCell.Value, "clm_sor_cd", "c.clm_sor_cd")
                QueryCell.Value = Replace(QueryCell.Value, "CLM_SOR_CD", "c.CLM_SOR_CD")
                QueryCell.Value = Replace(QueryCell.Value, "COLUMNNAME", "cp.COLUMNNAME")
                QueryCell.Value = Replace(QueryCell.Value, "CLM_ADJSTMNT_KEY", "cp.CLM_ADJSTMNT_KEY")
                QueryCell.Value = Replace(QueryCell.Value, "TABLENAME c", "TABLENAME cp")
                QueryCell.Value = Replace(QueryCell.Value, "c.LOAD_LOG_KEY", "cp.LOAD_LOG_KEY")
                QueryCell.Value = Replace(QueryCell.Value, "from TABLENAME cp" & Chr(10), "from TABLENAME cp" & Chr(10) & "left outer join CLM c" & Chr(10) & "on c.COLUMNNAME = cp.COLUMNNAME" & Chr(10))
                QueryCell.Value = Replace(QueryCell.Value, "--and RPTG_CLM_ADJDCTN_STTS_CD = '01' -- CLM table only" & Chr(10), "and length(trim(cp.CLM_ADJSTMNT_KEY)) < 1" & Chr(10) & "--and RPTG_CLM_ADJDCTN_STTS_CD = '01' -- CLM table only" & Chr(10))
                
            End If
            
        End If
        
        If SubjectArea = "Membership" Then
        
            QueryCell.Value = Replace(QueryCell.Value, "mbrshp_sor_cd", "MBRSHP_SOR_CD")
        
            If Protegrity = True Then
            
                QueryCell.Value = Replace(QueryCell.Value, "select MBRSHP_SOR_CD, COLUMNNAME, count(*) from TABLENAME", "select MBRSHP_SOR_CD, SCRTY_ACS_CNTRL.ANTM_MBR_NAME_DETOK(COLUMNNAME), count(*) from TABLENAME")
                
                If Rule = "UNK NA" Then
                
                    QueryCell.Value = Replace(QueryCell.Value, "--and COLUMNNAME in ('unk', 'na', '~na', 'UNKNOWN', 'unkwn', 'n/a' )", "--and TRIM(COLUMNNAME) in ('UNK', 'NA', '5ZWEU', 'KW9VH', 'UM9', 'N6', '~N6', 'JUCJ8F1', '3G6NYL', 'N/6', '~UD', 'GYPPTFZ', 'EW5ILV', 'U/D')")
                
                End If
            
            End If
            
        End If
'
        QueryCell.Value = Replace(QueryCell.Value, "--where ELL.PBLSH_DTM >= ((date-730)-(date-730) mod 100) + 1 --2 years where CLM_SOR_CD in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104'", _
        "--where ELL.PBLSH_DTM >= ((date-730)-(date-730) mod 100) + 1 --2 years where CLM_SOR_CD in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')")
        
'       Uncomment line if query will be run on CLM table
        If TableName = "CLM" _
        Then
        
            QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "--and RPTG", Chr(10) & "and RPTG")
            
        Else
        
            QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and RPTG", Chr(10) & "--and RPTG")
            
        End If
        
'       Uncomment line if query will be run on MBR table
        If TableName = "MBR" _
        Then
        
            QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "--and INDRCT_INSRT_CD in '0'", Chr(10) & "and INDRCT_INSRT_CD in '0'")
            
        Else
        
            QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and INDRCT_INSRT_CD in '0'", Chr(10) & "--and INDRCT_INSRT_CD in '0'")
                 
        End If
        
'       Uncomment line if query will be run on RCRD_STTS_CD column
        If ColumnName = "RCRD_STTS_CD" _
        Then
        
            QueryCell.Value = Replace(QueryCell.Value, "and RCRD_STTS_CD in ('ACT', 'MIG')", "--and RCRD_STTS_CD in ('ACT', 'MIG')")
            
        End If
        
        If ColumnName = "MBRSHP_SOR_CD" _
        Then
        
            QueryCell.Value = Replace(QueryCell.Value, "where MBRSHP_SOR_CD in ('808', '809', '815', '822', '823', '824', '886', '891', '892', '896', '994', '1104')", "--where mbrshp_sor_cd in ('808', '809', '815', '822', '823', '824', '886', '891', '892', '896', '994', '1104')")
            QueryCell.Value = Replace(QueryCell.Value, "and RCRD_STTS_CD in ('ACT', 'MIG')", "where RCRD_STTS_CD in ('ACT', 'MIG')")
            
        End If
        
        If ColumnName = "CLM_SOR_CD" _
        Then
        
            If Counter = 0 Then
            
                QueryCell.Value = Replace(QueryCell.Value, "where c.clm_sor_cd in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')", "--where c.clm_sor_cd in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')")
                
            Else
            
                QueryCell.Value = Replace(QueryCell.Value, "where c.clm_sor_cd in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')", "where --c.clm_sor_cd in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')")
                
            End If
            
            QueryCell.Value = Replace(QueryCell.Value, "and length(trim(cp.CLM_ADJSTMNT_KEY)) < 1", "--and length(trim(cp.CLM_ADJSTMNT_KEY)) < 1")
            
        End If
        
        If Right(QueryCell.Value, 1) <> Chr(10) Then QueryCell.Value = QueryCell.Value & Chr(10)
        .Range(QueryCell.Address).VerticalAlignment = xlTop
        .Range(QueryCell.Address).HorizontalAlignment = xlLeft
        .Range(QueryCell.Address).Font.Name = "Calibri"
        .Range(QueryCell.Address).Font.Size = 11
        
'       Replace placeholders with target table and column names
        QueryCell.Value = Replace(QueryCell.Value, "COLUMNNAME", ColumnName)
        QueryCell.Value = Replace(QueryCell.Value, "TABLENAME", TableName)
        
'
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and " & ColumnName & " like '%a%'", Chr(10) & "--and " & ColumnName & " like '%a%'")
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and ((" & ColumnName & " is null) or (" & ColumnName & " in ('')))", Chr(10) & "--and ((" & ColumnName & " is null) or (" & ColumnName & " in ('')))")
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and length(trim(" & ColumnName & ")) < 1", Chr(10) & "--and length(trim(" & ColumnName & ")) < 1")
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and " & ColumnName & " in ('~01', '~02', '~03')", "--and Chr(10) & " & ColumnName & " in ('~01', '~02', '~03')")
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and " & ColumnName & " in ('unk', 'na', '~na', 'unknown', 'unknwn', 'unkwn', 'n/a')", Chr(10) & "--and " & ColumnName & " in ('unk', 'na', '~na', 'unknown', 'unknwn', 'unkwn', 'n/a')")
        QueryCell.Value = Replace(QueryCell.Value, Chr(10) & "and " & ColumnName & " in ('0', '0.', '0.0', '0.00', '0.000', '.0', '.00', '.000', '.0000', '.00000', '.000000')", Chr(10) & "--and " & ColumnName & " in ('0', '0.', '0.0', '0.00', '0.000', '.0', '.00', '.000', '.0000', '.00000', '.000000')")

        Query = QueryCell.Value
        
    End With
    
    Debug.Print Query
    
'   Return query
    ReformatQuery = Query
    
End Function

Sub RunQuery(Query As String, SubjectArea As String)

    Dim RecordsetCounter As Integer, QueryRows As Integer, TotalsRowNum As Integer, LastTableRowNum As Integer, AutoSumColNum As Integer, Increment As Integer, StartRow As Integer, EndRow As Integer, InsertRowCounter As Integer
    Dim RowCounter As Integer, FormatEndRow As Integer, FormatEndCol As Integer, TextEndRow As Integer, ExtraCol As Integer
    Dim QueryResults() As Variant
    Dim TableStartPos As Range, QueryCell As Range, RangeEndPos As Range, InitTableSize As Range, NewTableSize As Range, TotalsRow As Range, IntersectDiff As Range, CompleteTable As Range, InitRowHeaders As Range
    Dim NewRowHeaders As Range, AutoSum As Range, AutoSumCol As Range
    Dim SQLCommand As ADODB.Command
    Dim SQLRecordset As ADODB.RecordSet
    
    On Error GoTo ErrHandler

    Increment = 0
    TextEndRow = 0
    ExtraCol = 0
    QueryRows = 0
    
    If SourceTemplate = True Then ExtraCol = 1
    
'   Add database to query
    Query = Replace(Query, "join ", "join EDW_ALLPHI.")
    Query = Replace(Query, "from ", "from EDW_ALLPHI.")
    
    SQLConnection.CommandTimeout = 2000
    
    Set SQLRecordset = CreateObject("ADODB.Recordset")
    If SQLRecordset.State = 1 Then SQLRecordset.Close
    SQLRecordset.CursorLocation = adUseClient
    
    With Sheets(SummarySheet)
        
        Set TableStartPos = .ListObjects(TableNames(Counter)).Range.Item(1)
        Set InitTableSize = .ListObjects(TableNames(Counter)).Range
        Set InitRowHeaders = .Range(TableStartPos.Offset(0, -1), .Cells(TableStartPos.Offset(1, -1).End(xlDown).Row, TableStartPos.Offset(1, -1).Column))
        TotalsRowNum = .ListObjects(TableNames(Counter)).Range.Rows.Count - 1
        
        .Range(TableStartPos.Offset(0, 1), TableStartPos.Offset(0, 1)).Value = ColumnName
        
        Set QueryCell = .Range(TableStartPos.Offset(-1, 0).Address)
        
        Application.Workbooks(2).Save

        If ModifyQuery = False Then
        
            SQLRecordset.Open Query, SQLConnection
            If SQLRecordset.State = 1 Then QueryRows = SQLRecordset.RecordCount
            
        End If
        
'       Uncomment line in query if more than 500 rows are returned
        If QueryRows > 500 Or QueryRows = 0 Or ModifyQuery = True Then
        
            ModifyQuery = True
        
            QueryCell.Value = Replace(QueryCell.Value, "--and " & ColumnName & " like '%a%'", "and " & ColumnName & " like '%a%'")
            QueryCell.Value = Replace(QueryCell.Value, "--and ((" & ColumnName & " is null) or (" & ColumnName & " in ('')))", "and ((" & ColumnName & " is null) or (" & ColumnName & " in ('')))")
            QueryCell.Value = Replace(QueryCell.Value, "--and length(trim(" & ColumnName & ")) < 1", "and length(trim(" & ColumnName & ")) < 1")
            QueryCell.Value = Replace(QueryCell.Value, "--and " & ColumnName & " in ('~01', '~02', '~03')", "and " & ColumnName & " in ('~01', '~02', '~03')")
            QueryCell.Value = Replace(QueryCell.Value, "--and lower(" & ColumnName & ") in ('unk', 'na', '~na', 'unknown', 'unknwn', 'unkwn', 'n/a')", "and lower(" & ColumnName & ") in ('unk', 'na', '~na', 'unknown', 'unknwn', 'unkwn', 'n/a')")
            QueryCell.Value = Replace(QueryCell.Value, "--and lower(" & ColumnName & ") in ('unk', 'na', '~na', 'unknown', 'unkwn', 'unknwn', 'n/a')", "and lower(" & ColumnName & ") in ('unk', 'na', '~na', 'unknown', 'unkwn', 'unknwn', 'n/a')")
            QueryCell.Value = Replace(QueryCell.Value, "--and " & ColumnName & " in ('0', '0.', '0.0', '0.00', '0.000', '.0', '.00', '.000', '.0000', '.00000', '.000000')", "and " & ColumnName & " in ('0', '0.', '0.0', '0.00', '0.000', '.0', '.00', '.000', '.0000', '.00000', '.000000')")
            QueryCell.Value = Replace(QueryCell.Value, "/*", "")
            QueryCell.Value = Replace(QueryCell.Value, "*/", "")
            QueryCell.Value = Replace(QueryCell.Value, "--and TRIM(" & ColumnName & ") in ('UNK', 'NA', '5ZWEU', 'KW9VH', 'UM9', 'N6', '~N6', 'JUCJ8F1', '3G6NYL', 'N/6', '~UD', 'GYPPTFZ', 'EW5ILV', 'U/D')", "and TRIM(" & ColumnName & ") in ('UNK', 'NA', '5ZWEU', 'KW9VH', 'UM9', 'N6', '~N6', 'JUCJ8F1', '3G6NYL', 'N/6', '~UD', 'GYPPTFZ', 'EW5ILV', 'U/D')")
            QueryCell.Value = Replace(QueryCell.Value, "--where c.CLM_SOR_CD in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')", "where --c.CLM_SOR_CD in ('808', '809', '822', '823', '824', '863', '869', '889', '892', '896', '898', '1037', '1104')")
            QueryCell.Value = Replace(QueryCell.Value, "--and cp." & ColumnName & " in ('unk', 'na', '~na', 'unknown', 'unkwn', 'n/a')", "cp." & ColumnName & " in ('unk', 'na', '~na', 'unknown', 'unkwn', 'n/a')")
            QueryCell.Value = Replace(QueryCell.Value, "--and length(trim(cp.CLM_ADJSTMNT_KEY)) < 1", "and length(trim(cp.CLM_ADJSTMNT_KEY)) < 1")

            Query = QueryCell.Value
            
'           Add database to query
            Query = Replace(Query, "join ", "join EDW_ALLPHI.")
            Query = Replace(Query, "from ", "from EDW_ALLPHI.")
            
            Debug.Print Query
            
'           Disconnent from database and reconnect to database with current query
            SQLConnection.CommandTimeout = 0
            
            QueryCell.Value = Query
        
            Application.Workbooks(2).Save
            
            Set SQLRecordset = CreateObject("ADODB.Recordset")
            If SQLRecordset.State = 1 Then SQLRecordset.Close
            SQLRecordset.CursorLocation = adUseClient
            SQLRecordset.Open Query, SQLConnection
            
'           Return rows for query results
            If SQLRecordset.State = 1 Then QueryRows = SQLRecordset.RecordCount
            
        End If
        
'        if more than 500 rows are returned
        If QueryRows <> 0 Then
        
            QueryResults() = SQLRecordset.GetRows
            
            StartRow = .ListObjects(TableNames(Counter)).Range.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlPrevious).Row
            EndRow = .ListObjects(TableNames(Counter)).Range.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlNext).Row + (UBound(QueryResults, 2) + 3)
            
            If Counter = 0 Then
            
                InitStartRow = StartRow
                InitEndRow = EndRow
                
            End If
            
'           Insert additional rows if query results extend beyond range of current table size
            If StartRow < EndRow Then
            
                If Counter = 0 Then
                
                    .Rows(StartRow + 1 & ":" & EndRow).EntireRow.Insert
                    .Rows(StartRow + 1 & ":" & EndRow).ClearFormats
                    
                End If
                
                For RowCounter = StartRow To EndRow - 1
                
                    Increment = Increment + 1
                    .Cells(RowCounter, TableStartPos.Offset(0, -1).Column).Value = .Cells(StartRow - 1, TableStartPos.Offset(0, -1).Column).Value + Increment
                    
                Next
                
            End If
            
'           Copy totals row to clipboard
            .ListObjects(TableNames(Counter)).ListRows(TotalsRowNum).Range.Copy
            
'           Resize target table to dimensions of query results
            If (UBound(TableNames) = 5 And Counter < 3) Or (UBound(TableNames) = 1 And Counter < 1) Then
            
                .ListObjects(TableNames(Counter)).Resize Range(TableNames(Counter) & "[#All]").Resize(UBound(QueryResults, 2) + 3)
                AutoSumColNum = 3 + ExtraCol
                FormatEndRow = 0
                
            Else
            
                .ListObjects(TableNames(Counter)).Resize Range(TableNames(Counter) & "[#All]").Resize(UBound(QueryResults, 2) + 3)
                AutoSumColNum = 2 + ExtraCol
                FormatEndRow = -1
            
            End If
            
'           Paste totals row to last row in table
            TotalsRowNum = .ListObjects(TableNames(Counter)).Range.Rows.Count - 1
            .ListObjects(TableNames(Counter)).ListRows(TotalsRowNum).Range.PasteSpecial
            
            If SubjectArea = "Claims" Then
            
                If Counter < 3 Then .Range(TableStartPos.Offset(TotalsRowNum, 3 + ExtraCol), TableStartPos.Offset(TotalsRowNum, 5 + ExtraCol)).ClearContents
                
            ElseIf SubjectArea = "Membership" Then
            
                If Counter = 0 Then .Range(TableStartPos.Offset(TotalsRowNum, 3 + ExtraCol), TableStartPos.Offset(TotalsRowNum, 5 + ExtraCol)).ClearContents
                
            End If
                
'           Paste header fields from recordset into first row of table
            SQLRecordset.MoveFirst
            
            For RecordsetCounter = 0 To SQLRecordset.Fields.Count - 1
            
                TableStartPos.Offset(0, RecordsetCounter).Value = SQLRecordset(RecordsetCounter).Name
            
            Next
            
'           Paste query results into table
            TableStartPos.Offset(1, 0).CopyFromRecordset SQLRecordset
            
            If QueryRows <= 2 Then TextEndRow = -1
            
            Range(TableStartPos.Offset(1, 0), TableStartPos.Offset(1, 0).End(xlDown).Offset(TextEndRow, 0)).TextToColumns DataType:=xlFixedWidth
        
            If (UBound(TableNames) = 5 And Counter < 3) Or (UBound(TableNames) = 1 And Counter < 1) Then
            
                .Range(TableStartPos.Offset(1, 1), TableStartPos.Offset(1, 1).End(xlDown).Offset(TextEndRow, 0)).TextToColumns DataType:=xlFixedWidth
                
            End If
            
        Else
        
            StartRow = .ListObjects(TableNames(Counter)).Range.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlPrevious).Row
            EndRow = .ListObjects(TableNames(Counter)).Range.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlNext).Row + 3
            
            If Counter = 0 Then
            
                InitStartRow = StartRow
                InitEndRow = EndRow
                
            End If
            
            .ListObjects(TableNames(Counter)).ListRows(TotalsRowNum).Range.Copy
        
            If (UBound(TableNames) = 5 And Counter < 3) Or (UBound(TableNames) = 1 And Counter < 1) Then
            
                .ListObjects(TableNames(Counter)).Resize Range(TableNames(Counter) & "[#All]").Resize(4)
                FormatEndCol = -3 + ExtraCol
                AutoSumColNum = 3 + ExtraCol
                
            Else
            
                .ListObjects(TableNames(Counter)).Resize Range(TableNames(Counter) & "[#All]").Resize(4)
                FormatEndCol = 0 + ExtraCol
                AutoSumColNum = 2 + ExtraCol
                    
            End If
            
            TotalsRowNum = .ListObjects(TableNames(Counter)).Range.Rows.Count - 1
            .ListObjects(TableNames(Counter)).ListRows(TotalsRowNum).Range.PasteSpecial
        
            If SubjectArea = "Claims" Then
        
                If Counter < 3 Then .Range(TableStartPos.Offset(TotalsRowNum, 3 + ExtraCol), TableStartPos.Offset(TotalsRowNum, 5 + ExtraCol)).ClearContents
            
            ElseIf SubjectArea = "Membership" Then
        
                If Counter = 1 Then .Range(TableStartPos.Offset(TotalsRowNum, 3 + ExtraCol), TableStartPos.Offset(TotalsRowNum, 5 + ExtraCol)).ClearContents
            
            End If
        
        End If
        
        Set NewTableSize = .ListObjects(TableNames(Counter)).Range
        
        LastTableRowNum = .ListObjects(TableNames(Counter)).Range.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlPrevious).Row
        
        Set NewRowHeaders = .Range(TableStartPos.Offset(0, -1), .Cells(LastTableRowNum, TableStartPos.Offset(1, -1).Column))
        
'       Clear cell contents extending outside current table
        If StartRow >= EndRow Then

            Set IntersectDiff = DisUnion(InitTableSize, NewTableSize)
            IntersectDiff.Clear
            
            Set IntersectDiff = DisUnion(InitRowHeaders, NewRowHeaders)
            IntersectDiff.Clear
            
        End If
        
        If InitStartRow >= InitEndRow And Counter = 2 Then .Rows(InitEndRow + 1 & ":" & InitStartRow + 1).EntireRow.Delete
        
'       Recalculate autosum
        Set AutoSumCol = .ListObjects(TableNames(Counter)).ListColumns(AutoSumColNum).Range
        Set AutoSumCol = AutoSumCol.Resize(AutoSumCol.Rows.Count - 1)
        Set AutoSum = AutoSumCol.End(xlDown)
        
'       Reapply table formatting
        If QueryRows > 2 Then
        
            AutoSum.Formula = "=SUM(" & AutoSumCol.Address(False, False) & ")"
           
            .Range(TableStartPos.Offset(1, 0), TableStartPos.Offset(2, 0).End(xlToRight)).Copy
            .Range(TableStartPos.Offset(3, 0), TableStartPos.Offset(3, 0).End(xlDown).Offset(FormatEndRow, 0).End(xlToRight)).PasteSpecial Paste:=xlPasteFormats
            
            TableStartPos.Offset(1, -1).Copy
            .Range(TableStartPos.Offset(2, -1), TableStartPos.Offset(2, -1).End(xlDown)).PasteSpecial Paste:=xlPasteFormats
            
        Else
        
            AutoSum.ClearContents
        
            .Range(TableStartPos.Offset(1, 0), TableStartPos.Offset(2, 0).End(xlToRight).Offset(0, FormatEndCol)).ClearContents
            
        End If
        
'       Reset table borders
        Set CompleteTable = Union(NewRowHeaders, NewTableSize)
        
        NewRowHeaders.Borders.LineStyle = xlLineStyleNone
        NewTableSize.Borders.LineStyle = xlLineStyleNone
        
        CompleteTable.Borders.Weight = xlThin
        
        CompleteTable.Borders(xlEdgeBottom).LineStyle = xlContinuous
        CompleteTable.Borders(xlEdgeLeft).LineStyle = xlContinuous
        CompleteTable.Borders(xlEdgeRight).LineStyle = xlContinuous
        CompleteTable.Borders(xlEdgeTop).LineStyle = xlContinuous
        CompleteTable.Borders(xlEdgeBottom).Weight = xlMedium
        CompleteTable.Borders(xlEdgeLeft).Weight = xlMedium
        CompleteTable.Borders(xlEdgeRight).Weight = xlMedium
        CompleteTable.Borders(xlEdgeTop).Weight = xlMedium
    
    End With
        
    SQLRecordset.Close
    Set SQLRecordset = Nothing

ErrHandler:

    Debug.Print Err.Number & ": " & Err.Description
    
    If Err.Number = -2147217871 Then

        Err.Clear
        ModifyQuery = True
        Resume Next

    End If
    
    If Err.Number = -2147467259 Then

        Err.Clear
        ModifyQuery = True
        Resume Next

    End If
    
    If Err.Number = 9 Then

        Err.Clear
        Resume Next

    End If
    
    Debug.Print Query

End Sub

Sub InsertRows()

    Dim Counter As Integer, RowCounter As Integer, SORCounter As Integer, StartRow As Integer, EndRow As Integer, InsertRowCount As Integer, ExtraCol As Integer
    Dim InsertRowCountSplit() As String
    
    ExtraCol = 0
    
    If SourceTemplate = True Then ExtraCol = 1

    For Counter = 1 To Sheets.Count
    
        For SORCounter = 0 To 16
        
            If Sheets(Counter).Name = SORName(SORCounter) & "_" & SORNumber(SORCounter) Then
            
                With Sheets(SORName(SORCounter) & "_" & SORNumber(SORCounter))
                
                    .Select
                
                    If Left(.Range("A24").Value, 1) = "T" Then
                    
                        StartRow = 22
                        
                        InsertRowCountSplit() = Split(CStr(Range("A24").Value))
                        
                        If InsertRowCountSplit(8) <> "above" Then
                        
                            InsertRowCount = CInt(InsertRowCountSplit(8))
                            EndRow = StartRow + InsertRowCount - 1
                            .Rows(StartRow & ":" & EndRow).EntireRow.Insert
                            
                            .Range("A21:S21").Copy
                            .Range("A22", "A" & (22 + InsertRowCount)).PasteSpecial xlPasteAll
                            
                            EndRow = .Range("A18").End(xlDown).Row
                            
                            If SourceTemplate = True Then
                            
                                .Range(.Range("D18"), .Range("D" & EndRow)).TextToColumns DataType:=xlFixedWidth
                                
                            Else
                            
                                .Range(.Range("C18"), .Range("C" & EndRow)).TextToColumns DataType:=xlFixedWidth
                                
                            End If
                            
'                            If Rule <> "Zero" And Rule <> "Check All Invalid Char" And Rule <> "Check Limited Invalid Char" Then
'
'                                .Range(.Range("H18"), .Range("H" & EndRow)).TextToColumns DataType:=xlFixedWidth
'                                .Range(.Range("M18"), .Range("M" & EndRow)).TextToColumns DataType:=xlFixedWidth
'
'                            End If
                            
                        End If
                        
                        StartRow = 19
                        EndRow = StartRow + 2 + InsertRowCount
                        
                        For RowCounter = StartRow To EndRow
                        
                            .Cells(RowCounter, 3 + ExtraCol).Errors(3).Ignore = True
                            .Cells(RowCounter, 8 + ExtraCol).Errors(3).Ignore = True
                            .Cells(RowCounter, 13 + ExtraCol).Errors(3).Ignore = True
                            
                        Next
                        
                    End If
                    
                End With
                
                Exit For
                
            End If
            
        Next
        
    Next
    
End Sub

Sub GetDPResultStatus()

    Dim ExtraRow As Integer, TextPos As Integer, DPRSPercent As Double
    Dim CellText As String

    ExtraRow = 0
    TextPos = 0
    If SubjectArea = "Claims" Then ExtraRow = 2

    Sheets("Formula Sheet").Range("B4:B16").Copy
    
    With Sheets("Profiling Analysis Overview")
    
'    Range(Selection, Selection.End(xlDown)).Select
        
        .Range("D16:D" & CStr(27 + ExtraRow)).PasteSpecial xlPasteValues
        .Range("C16:D" & CStr(27 + ExtraRow)).Font.Bold = True
        
        For Counter = 16 To 27 + ExtraRow
        
            .Cells(Counter, 14).Value = "NA"
            .Cells(Counter, 4).Value = Replace(.Cells(Counter, 4).Value, "correct", "Correct")
            
            If ScorecardDB = "" And ScorecardLastRun = "" _
            Then .Cells(Counter, 3).Value = "Cannot View or Run Scorecard"
            
            CellText = .Cells(Counter, 4).Value
            
            For TextPos = 1 To Len(CellText)
            
                If Mid(CellText, TextPos, 1) Like "#" Then
                
                    DPRSPercent = CInt(Mid(CellText, TextPos, 3))
                    Exit For
                    
                End If
                
            Next
            
'            TextPos = InStr(CellText, "%")

            If DPRSPercent < 100 Then
            
'                "The CLM_NBR in EDWard is incorrectly populated with Filler values for some sources in EDWard.  Some sources are correctly populated without Filler values in EDWard. "
                
            End If
            
            If DPRSPercent > 99.5 Then
            
                .Cells(Counter, 4).Characters(TextPos, 4).Font.Color = RGB(0, 176, 80)
            
            ElseIf DPRSPercent > 95 And DPRSPercent <= 99.5 Then
            
                .Cells(Counter, 4).Characters(TextPos, 4).Font.Color = RGB(255, 192, 0)
            
            ElseIf DPRSPercent >= 0 And DPRSPercent <= 95 Then
            
                .Cells(Counter, 4).Characters(TextPos, 4).Font.Color = RGB(192, 0, 0)
            
            End If
            
            If .Cells(Counter, 2).Value = "CAREMARK_1037" Then
            
                .Cells(Counter, 8).Value = "TBD"
                .Cells(Counter, 14).Value = "Verify if source can be removed from scorecard and template or if reporting will be needed and this source should be profiled."
                
            End If
            
        Next
        
        
        CellText = .Range("A11").Value
        TextPos = InStr(CellText, "correctly")
'        .Range("A11").Characters(TextPos, 9).Insert ("incorrectly")
        CellText = .Range("A11").Value
        TextPos = InStr(CellText, "without")
'        .Range("A11").Characters(TextPos, 7).Insert ("with")
        CellText = .Range("A11").Value
        TextPos = InStr(CellText, "all")
'        .Range("A11").Characters(TextPos, 3).Insert (IncorrectNum)
        'populated with Filler values for some sources in EDWard.  Some sources are correctly populated without Filler values in EDWard.")
'       "The MAIL_ORDR_CD in EDWard is correctly populated without UNK or NA values for all sources in EDWard."
        
        .Select
'        .Range("D16:D28").Columns("D").EntireColumn.AutoFit
        .Columns("D:E").AutoFit
        
        .Range("A13:N28").WrapText = True
        
    End With
    
End Sub

Sub FindMapping()

    Dim Row As Integer
    Dim Column As Integer
    Dim Rule As Integer
    Dim Metric As Integer
    Dim Lookup As String
    Dim Rules(1 To 15) As String
    Dim Metrics(1 To 20) As String
    Dim cell As Range
    Dim FirstFound As Range
    Dim InsertRowPos As Integer
    Dim FirstRowFlag As Boolean

    Row = 2
    Metric = 1
    InsertRowPos = 1
    
    Do
        
        FirstRowFlag = True
        
        For Rule = 1 To 15
        
            Rules(Rule) = ""
            
        Next

        Lookup = Worksheets("SP List Data").Cells(Row, 5).Value

        If Lookup = "" Then Exit Do

        With Worksheets("CDE Rules Metrics").Range("D2:D572")
        
            Set cell = .Find(Lookup, LookIn:=xlValues, Lookat:=xlPart)
            
        End With
                
        If Not (cell Is Nothing) Then
                
                Application.Goto Reference:=cell

                For Rule = 1 To 15
            
                    Column = Rule
                    If cell.Offset(0, Column + 4).Value = "Y" Then Rules(Rule) = Cells(1, Column + 8).Value
                
                Next
        
            With Worksheets("Metrics").Range("A2:A901")
        
                For Rule = 1 To 15
                
                    If Rules(Rule) <> "" Then
            
                        Set cell = .Find(Rules(Rule), LookIn:=xlValues, Lookat:=xlWhole)
                
                        Set FirstFound = cell
                    
                        If Not (cell Is Nothing) Then
                    
                            Application.Goto Reference:=cell
                
                            Metrics(Metric) = ActiveCell.Offset(0, 1).Value
                            Metric = Metric + 1
            
                            Do
                    
                                Set cell = .Find(Rules(Rule), After:=cell, LookIn:=xlValues, Lookat:=xlWhole)
                                
                                If cell.Address = FirstFound.Address Then Exit Do
                        
                                If Not (cell Is Nothing) Then
                            
                                    Application.Goto Reference:=cell
                        
                                    If InStr(ActiveCell.Offset(0, 1).Value, "Row_Valid") = 0 Then
                                    
                                        Metrics(Metric) = ActiveCell.Offset(0, 1).Value
                                        Metric = Metric + 1
                                        
                                    End If
                                    
                                End If
                    
                            Loop
                        
                        End If
                
                        For Metric = 1 To 20
                        
                            If Metrics(Metric) = "" Then Exit For
                            
                            If FirstRowFlag = True Then
                                
                                Worksheets("SP List Data").Cells(Row, 18).Value = Rules(Rule)
                                Worksheets("SP List Data").Cells(Row, 19).Value = Metrics(Metric)
                                
                                FirstRowFlag = False
                                    
                            Else
        
                                Worksheets("SP List Data").Rows(Row).EntireRow.Copy
                                Worksheets("SP List Data").Rows(Row).Offset(InsertRowPos).EntireRow.Insert
                                Worksheets("SP List Data").Cells(Row + InsertRowPos, 18).Value = Rules(Rule)
                                Worksheets("SP List Data").Cells(Row + InsertRowPos, 19).Value = Metrics(Metric)
                    
                                InsertRowPos = InsertRowPos + 1
                                    
                            End If
                    
                        Next
                
                        For Metric = 1 To 20
        
                            If Metrics(Metric) = "" Then Exit For
                            Metrics(Metric) = ""
            
                        Next
                        
                    End If
                
                    Metric = 1
                
                Next
            
            End With
            
        End If
        
        Row = Row + InsertRowPos
        InsertRowPos = 1
        
    Loop
        
End Sub

Public Function DisUnion(keep As Range, remove As Range) As Range

    Dim rng_output As Range

    Dim cell As Range
    For Each cell In keep

        'check if given cell is in range to remove
        If Intersect(cell, remove) Is Nothing Then

            'this builds the output and handles first case
            If rng_output Is Nothing Then
            
                Set rng_output = cell
                
            Else
            
                Set rng_output = Union(rng_output, cell)
                
            End If
            
        End If
        
    Next cell

    Set DisUnion = rng_output

End Function
